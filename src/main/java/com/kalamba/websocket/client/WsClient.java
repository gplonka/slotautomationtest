package com.kalamba.websocket.client;

import java.net.URI;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

public class WsClient extends WebSocketClient {

  private MessageHandler messageHandler;

  public WsClient(URI serverURI) {
    super(serverURI);
  }

  @Override
  public void onOpen(ServerHandshake handshakedata) {
    System.out.println("Opened connection\n");
  }

  @Override
  public void onMessage(String message) {
    messageHandler.handleMessage(message);

  }

  @Override
  public void onClose(int code, String reason, boolean remote) {
    System.out.println(
        "\nConnection closed by " + (remote ? "remote peer" : "us") + " Code: " + code + " Reason: "
            + reason);
  }

  @Override
  public void onError(Exception ex) {
    ex.printStackTrace();
  }

  public void setMessageHandler(MessageHandler msgHandler) {
    this.messageHandler = msgHandler;
  }


  public interface MessageHandler {

    void handleMessage(String message);
  }
}
