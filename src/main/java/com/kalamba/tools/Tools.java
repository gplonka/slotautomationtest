package com.kalamba.tools;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import com.kalamba.logging.properties.LoadProperties;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.URISyntaxException;
import java.security.CodeSource;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;
import org.yaml.snakeyaml.Yaml;


public class Tools {


  public static String fillParameter(String field, Map<String, String> paramValueMap) {

    for (String parameter : paramValueMap.keySet()) {
      field = Tools.fill(field, Map.of(parameter, paramValueMap.get(parameter)));
    }

    return field;
  }

  public static Object mapYamlToObject(String yamlPath, Object object) {

    if (yamlPath.equals(LoadProperties.propertiesPath)) {
      try (InputStream is = Tools.class.getResourceAsStream(yamlPath)) {
        object = new Yaml().loadAs(is, object.getClass());
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    } else {
      try (Reader r = new FileReader(yamlPath)) {
        object = new Yaml().loadAs(r, object.getClass());
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }

    return object;
  }

  private static String fill(String template, Map<?, ?> arguments) {

    return Pattern.compile("[\\[]([^}]*?)[\\]]").matcher(template).replaceAll(
        match -> Optional.ofNullable(arguments.get(match.group(1))).map(Object::toString)
            .orElseGet(match::group)
    );
  }

  public static String getElement(String json, String path) {
    return JsonPath.parse(json).read("$." + path).toString();
  }

  public static Boolean isPathExist(String json, String path) {
    Configuration conf = Configuration.defaultConfiguration()
        .addOptions(Option.SUPPRESS_EXCEPTIONS);
    return JsonPath.using(conf).parse(json).read("$." + path) != null;
  }

  public static String extractString(String text, String leftToken, String rightToken) {

    return text.split(leftToken)[1].split(rightToken)[0];
  }

  public static void createFile(String baseName, String path, String content) {

    try (Writer writer = new BufferedWriter(new OutputStreamWriter(
        new FileOutputStream(path + baseName, true)))) {

      writer.append(content);
      writer.append("\n###########################\n");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static String createDirectory(String directoryName) {

    File jarFile;
    String jarDir = "";
    try {
      CodeSource codeSource = Tools.class.getProtectionDomain().getCodeSource();
      jarFile = new File(codeSource.getLocation().toURI().getPath());
      jarDir = jarFile.getParentFile().getPath();
      new File(jarDir + "/" + directoryName).mkdir();

    } catch (URISyntaxException e) {
      e.printStackTrace();
    }

    return jarDir + "/" + directoryName + "/";
  }
}



