package com.kalamba.http.client;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class HttpClient {

  private java.net.http.HttpClient client;
  private String contentType;
  private String uri;


  public HttpClient(String uri) {

    contentType = "application/x-www-form-urlencoded";
    this.uri = uri;
    client = java.net.http.HttpClient.newHttpClient();
  }

  public HttpClient(String uri, String contentType) {

    this(uri);
    this.contentType = contentType;
  }

  public String[] get() {

    HttpResponse<String> response;

    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create(uri))
        .GET()
        .build();

    try {

      response = client.send(request,
          HttpResponse.BodyHandlers.ofString());
    } catch (IOException | InterruptedException e) {
      throw new RuntimeException(e);
    }

    String[] resp = new String[2];

    resp[0] = response.headers().toString();
    resp[1] = response.body();

    return resp;
  }

  public Integer post(String body) {

    HttpResponse<String> response;
    HttpRequest request = HttpRequest.newBuilder()
        .header("Content-Type", contentType)
        .uri(URI.create(uri))
        .POST(HttpRequest.BodyPublishers.ofString(body))
        .build();

    try {
      response = client.send(request,
          HttpResponse.BodyHandlers.ofString());
    } catch (IOException | InterruptedException e) {
      throw new RuntimeException(e);
    }

    return response.statusCode();
  }
}
