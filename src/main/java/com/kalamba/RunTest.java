package com.kalamba;

import com.kalamba.logging.properties.LoadProperties;
import com.kalamba.logging.properties.Properties;
import com.kalamba.slot.service.SlotImpl;
import com.kalamba.test.slot.game.BalanceTest;
import com.kalamba.test.slot.game.BreakingFreeSpinTest;
import com.kalamba.test.slot.game.SlotTest;

public class RunTest {

  public static void main(String[] args) {

    if (args.length > 0) {
      Properties prop = LoadProperties.loadProperties(args[0]);

      SlotTest test1 = new BalanceTest(new SlotImpl(prop), prop);
      SlotTest test2 = new BreakingFreeSpinTest(new SlotImpl(prop), prop);

      test1.test();
      test2.test();
    }
  }
}





