package com.kalamba.test.slot.game;

import com.kalamba.http.client.HttpClient;
import com.kalamba.logging.properties.Properties;
import com.kalamba.slot.service.Slot;

public abstract class SlotTest {

  protected Slot slot;
  protected Properties properties;
  protected SaveTest saveTest;
  protected HttpClient httpClient;

  public SlotTest(Slot slot, Properties properties, SaveTest saveTest, HttpClient httpClient) {
    this.slot = slot;
    this.properties = properties;
    this.saveTest = saveTest;
    this.httpClient = httpClient;
  }


  public void forceOutcome(String request) {
    httpClient.post(request);
  }

  public abstract void test();
}
