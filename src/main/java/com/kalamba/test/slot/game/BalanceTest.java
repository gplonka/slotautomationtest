package com.kalamba.test.slot.game;

import com.kalamba.http.client.HttpClient;
import com.kalamba.logging.properties.LoadProperties;
import com.kalamba.logging.properties.Properties;
import com.kalamba.slot.service.Slot;
import com.kalamba.tools.Tools;
import java.math.BigDecimal;

public class BalanceTest extends SlotTest {


  private BigDecimal initialBalance;
  private BigDecimal balanceAfterTest;
  private Integer spinNumber;
  private BigDecimal baseBet;
  private Integer betMultiplier;
  private BigDecimal countedBalanceAfterTest;
  private BigDecimal balanceAfterFreeSpins;
  private String openGameResp;


  public BalanceTest(Slot slot, Properties p) {
    this(slot, p, new SlotSaveTest(), new HttpClient(LoadProperties.forceOutcomeUri));
  }

  public BalanceTest(Slot slot, Properties p, SaveTest saveTest, HttpClient httpClient) {
    super(slot, p, saveTest, httpClient);
    this.saveTest = saveTest;
    this.httpClient = httpClient;
  }


  @Override
  public void test() {

    openGame();
    String result = completeBalanceTest();
    saveTest.save(result);
    initialBalance = balanceAfterTest;
    result = freeSpinsBalanceTest();
    saveTest.save(result);

    slot.closeGame();

  }

  public String completeBalanceTest() {

    BigDecimal totalSpinWin = baseSpinRound();

    BigDecimal totalBet = baseBet.multiply(new BigDecimal(spinNumber))
        .multiply(new BigDecimal(betMultiplier));

    countedBalanceAfterTest = initialBalance.subtract(totalBet).add(totalSpinWin);

    if (balanceAfterTest.equals(countedBalanceAfterTest)) {
      return completeBalanceTestInfo(balanceAfterTest, totalSpinWin, totalBet,
          "Balance is correct");
    } else {
      return completeBalanceTestInfo(balanceAfterTest, totalSpinWin, totalBet,
          "Balance is incorrect");
    }
  }

  public String freeSpinsBalanceTest() {

    forceOutcome(LoadProperties.getForceOutcomePostBody(properties));

    BigDecimal singleBet = baseBet.multiply(new BigDecimal(betMultiplier));
    String spinResp = slot.baseSpin();
    BigDecimal singleSpinWin = new BigDecimal(Tools.getElement(spinResp, LoadProperties.winPath));
    BigDecimal totalFreeSpinWin = freeSpinRound();

    countedBalanceAfterTest = initialBalance.subtract(singleBet).add(totalFreeSpinWin)
        .add(singleSpinWin);

    if (balanceAfterFreeSpins.equals(countedBalanceAfterTest)) {
      return freeSpinsBalanceTestInfo("Balance is correct", totalFreeSpinWin);
    } else {
      return freeSpinsBalanceTestInfo("Balance is incorrect", totalFreeSpinWin);
    }
  }

  private BigDecimal baseSpinRound() {

    BigDecimal totalSpinWin = new BigDecimal("0");
    BigDecimal singleSpinWin;
    BigDecimal totalFreeSpinWin;
    String spinResp = "";
    spinNumber = properties.getParameters().getSpinNumber();

    for (int round = 0; round < spinNumber; round++) {

      spinResp = slot.baseSpin();
      singleSpinWin = new BigDecimal(Tools.getElement(spinResp, LoadProperties.winPath));
      totalSpinWin = totalSpinWin.add(singleSpinWin);

      if (Tools.isPathExist(spinResp, LoadProperties.freeSpinPath)) {
        totalFreeSpinWin = freeSpinRound();
        totalSpinWin = totalSpinWin.add(totalFreeSpinWin);
      }
    }

    balanceAfterTest = new BigDecimal(Tools.getElement(spinResp, LoadProperties.balancePath));

    return totalSpinWin;
  }

  private BigDecimal freeSpinRound() {

    Integer freeSpinRemaining;
    BigDecimal win;
    BigDecimal totalFreeSpinWin = new BigDecimal("0");
    String freeResp = "";

    do {

      freeResp = slot.freeSpin();
      win = new BigDecimal(Tools.getElement(freeResp, LoadProperties.winPath));
      totalFreeSpinWin = totalFreeSpinWin.add(win);
      freeSpinRemaining = Integer
          .parseInt(Tools.getElement(freeResp, LoadProperties.freeSpinRemainingPath));

    } while (freeSpinRemaining != 0);

    balanceAfterFreeSpins = new BigDecimal(Tools.getElement(freeResp, LoadProperties.balancePath));

    return totalFreeSpinWin;
  }

  public BigDecimal getCountedBalanceAfterTest() {
    return countedBalanceAfterTest;
  }

  private void openGame() {

    openGameResp = slot.openGame();
    LoadProperties.loadGameRequestsParameters(openGameResp, properties);

    baseBet = properties.getParameters().getBaseBet();
    betMultiplier = properties.getParameters().getBetMultiplier();
    initialBalance = new BigDecimal(Tools.getElement(openGameResp, LoadProperties.balancePath));
  }

  private String testBalanceInfo(BigDecimal balanceAfterTest) {

    return "Expected value: " + countedBalanceAfterTest +
        "\nFound value: " + balanceAfterTest;

  }

  private String completeBalanceTestInfo(BigDecimal balanceAfterTest, BigDecimal totalSpinWin,
      BigDecimal totalBet, String result) {

    return "completeBalanceTestInfo" +
        "\nInitial balance: " + initialBalance +
        "\nTotal bet: " + totalBet +
        "\nTotal wins: " + totalSpinWin +
        "\n" + result + "\n" + testBalanceInfo(balanceAfterTest);

  }

  private String freeSpinsBalanceTestInfo(String result, BigDecimal totalFreeSpinWin) {

    return "freeSpinsBalanceTestInfo" +
        "\n" + result + "\n" + "totalFreeSpinWin: " + totalFreeSpinWin +
        "\n" + testBalanceInfo(balanceAfterFreeSpins);
  }
}