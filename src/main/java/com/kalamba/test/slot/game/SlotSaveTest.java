package com.kalamba.test.slot.game;

import com.kalamba.tools.Tools;

public class SlotSaveTest implements SaveTest {

  private String path;

  public SlotSaveTest() {
    path = Tools.createDirectory("Test_result");
  }

  public void save(String testResult) {
    Tools.createFile("Test_result", path, testResult);
  }
}
