package com.kalamba.test.slot.game;

import com.kalamba.http.client.HttpClient;
import com.kalamba.logging.properties.LoadProperties;
import com.kalamba.logging.properties.Properties;
import com.kalamba.slot.service.Slot;
import com.kalamba.tools.Tools;

public class BreakingFreeSpinTest extends SlotTest {


  public BreakingFreeSpinTest(Slot slot, Properties p) {
    this(slot, p, new SlotSaveTest(), new HttpClient(LoadProperties.forceOutcomeUri));
  }

  public BreakingFreeSpinTest(Slot slot, Properties p, SaveTest saveTest, HttpClient httpClient) {
    super(slot, p, saveTest, httpClient);
    this.saveTest = saveTest;
    this.httpClient = httpClient;
  }


  @Override
  public void test() {

    String result = freeSpinRemainingAfterBreakTest();
    saveTest.save(result);

    slot.closeGame();
  }

  private String freeSpinRemainingAfterBreakTest() {

    forceOutcome(LoadProperties.getForceOutcomePostBody(properties));

    String openGameResp = slot.openGame();
    LoadProperties.loadGameRequestsParameters(openGameResp, properties);

    String spinResp = slot.baseSpin();

    Integer[] freeSpinRemainingsBeforeAfterBreak = freeSpinRound(spinResp);

    Integer freeSpinRemainingBefore = freeSpinRemainingsBeforeAfterBreak[0];
    Integer freeSpinRemainingAfter = freeSpinRemainingsBeforeAfterBreak[1];

    if (freeSpinRemainingBefore.equals(freeSpinRemainingAfter)) {

      return "After re open game freeSpinRemaining value is correct"
          + freeSpinRemainingAfterBreakTestInfo(freeSpinRemainingBefore, freeSpinRemainingAfter);
    } else {
      return "After re open game freeSpinRemaining value is incorrect"
          + freeSpinRemainingAfterBreakTestInfo(freeSpinRemainingBefore, freeSpinRemainingAfter);
    }
  }

  private Integer[] freeSpinRound(String spinResponse) {

    Integer freeSpinRemaining = Integer
        .parseInt(Tools.getElement(spinResponse, LoadProperties.freeSpinRemainingPath));

    Integer freeSpinRemainingAfter = 0;
    Integer freeSpinRemainingBefore = 0;

    Integer breakPoint = freeSpinRemaining - 2;
    Integer freeSpinRound = 0;
    String freeResp = "";

    do {

      if ((freeSpinRound++).equals(breakPoint)) {

        freeSpinRemainingBefore = freeSpinRemaining;

        slot = slot.reOpenGame();
        String resp = slot.openGame();

        freeSpinRemainingAfter = Integer
            .parseInt(Tools.getElement(resp, LoadProperties.freeSpinRemainingPath));
      }

      freeResp = slot.freeSpin();

      freeSpinRemaining = Integer
          .parseInt(Tools.getElement(freeResp, LoadProperties.freeSpinRemainingPath));


    } while (freeSpinRemaining != 0);

    Integer[] freeSpinRemainingsBeforeAfterBreak = new Integer[2];

    freeSpinRemainingsBeforeAfterBreak[0] = freeSpinRemainingBefore;
    freeSpinRemainingsBeforeAfterBreak[1] = freeSpinRemainingAfter;

    return freeSpinRemainingsBeforeAfterBreak;
  }

  private String freeSpinRemainingAfterBreakTestInfo(Integer freeSpinRemainingBefore,
      Integer freeSpinRemainingAfter) {

    return "\nFree spin remaining before re open: " + freeSpinRemainingBefore +
        "\nFree spin remaining after re open: " + freeSpinRemainingAfter;
  }
}
