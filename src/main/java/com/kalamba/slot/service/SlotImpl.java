package com.kalamba.slot.service;

import com.kalamba.logging.properties.LoadProperties;
import com.kalamba.logging.properties.Properties;
import com.kalamba.tools.Tools;
import com.kalamba.websocket.client.WsClient;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.Semaphore;

public class SlotImpl implements Slot {


  private Semaphore waitForServerResponse = new Semaphore(0);
  private String serverResponse;
  private WsClient webSocketClient;

  private Properties properties;

  public SlotImpl(Properties p) {
    this.properties = p;
    connectWebSocket();
  }


  private void connectWebSocket() {

    try {

      webSocketClient = new WsClient(new URI(properties.getWebSocketUri()));
      addRgsMessageHandler(webSocketClient);
      if (webSocketClient.connectBlocking()) {
        webSocketClient.send(properties.getAuthenticateRequest());
      }
    } catch (URISyntaxException | InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

  private void sendRequestWithSynch(String req) {

    webSocketClient.send(req);
    waitForServerResponse.acquireUninterruptibly();

  }

  @Override
  public Slot reOpenGame() {

    closeGame();

    return new SlotImpl(properties);
  }

  @Override
  public String openGame() {

    sendRequestWithSynch(properties.getOpenGameRequest());

    return serverResponse;
  }

  @Override
  public String baseSpin() {

    sendRequestWithSynch(properties.getBaseSpinRequest());

    return serverResponse;
  }

  @Override
  public String freeSpin() {

    sendRequestWithSynch(properties.getFreeSpiRequest());

    return serverResponse;
  }

  @Override
  public void closeGame() {

    webSocketClient.close();
  }


  private void addRgsMessageHandler(WsClient ws) {

    ws.setMessageHandler(new WsClient.MessageHandler() {

      public void handleMessage(String message) {

        if (Tools.isPathExist(message, LoadProperties.eventPath)) {
          String nodeName = Tools.getElement(message, LoadProperties.eventPath);

          if (LoadProperties.SPIN_RESULT.equals(nodeName) || LoadProperties.OPEN_GAME
              .equals(nodeName)) {
            serverResponse = message;
            waitForServerResponse.release();
          }
        }
      }
    });
  }
}
