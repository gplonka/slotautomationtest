package com.kalamba.slot.service;

public interface Slot {

  String openGame();
  String baseSpin();
  String freeSpin();
  void closeGame();
  Slot reOpenGame();
}
