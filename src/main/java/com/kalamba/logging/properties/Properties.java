package com.kalamba.logging.properties;

import com.kalamba.http.client.HttpClient;
import com.kalamba.tools.Tools;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Properties {

  private String webSocketUri;
  private String baseSpinRequest;
  private String openGameRequest;
  private String authenticateRequest;
  private String freeSpiRequest;
  private String httpUri;
  private Parameters parameters;


  public String getHttpUri() {
    return httpUri;
  }

  public void setHttpUri(String httpUri) {
    this.httpUri = httpUri;
  }

  public String getWebSocketUri() {
    return webSocketUri;
  }

  public void setWebSocketUri(String webSocketUri) {
    this.webSocketUri = webSocketUri;
  }

  public String getBaseSpinRequest() {
    return baseSpinRequest;
  }

  public void setBaseSpinRequest(String baseSpinRequest) {
    this.baseSpinRequest = baseSpinRequest;
  }

  public String getOpenGameRequest() {
    return openGameRequest;
  }

  public void setOpenGameRequest(String openGameRequest) {
    this.openGameRequest = openGameRequest;
  }

  public String getAuthenticateRequest() {
    return authenticateRequest;
  }

  public void setAuthenticateRequest(String authenticateRequest) {
    this.authenticateRequest = authenticateRequest;
  }

  public String getFreeSpiRequest() {
    return freeSpiRequest;
  }

  public void setFreeSpiRequest(String freeSpiRequest) {
    this.freeSpiRequest = freeSpiRequest;
  }


  public Parameters getParameters() {
    return parameters != null ? parameters : new Parameters();
  }

  public void setParameters(Parameters parameters) {
    this.parameters = parameters;
  }


  public static class Parameters {

    private String gameCode;
    private String username;
    private Integer spinNumber;
    private BigDecimal baseBet;
    private Integer betMultiplier;
    private String currency;
    private String token;
    private Map<String, String> parametersMap;

    public String getCurrency() {
      return currency;
    }

    public void setCurrency(String currency) {
      this.currency = currency;
    }

    public String getToken() {
      return token;
    }

    public void setToken(String token) {
      this.token = token;
    }

    public Integer getBetMultiplier() {
      return betMultiplier;
    }

    public void setBetMultiplier(Integer betMultiplier) {
      this.betMultiplier = betMultiplier;
    }

    public String getGameCode() {
      return gameCode;
    }

    public void setGameCode(String gameCode) {
      this.gameCode = gameCode;
    }

    public String getUsername() {
      return username;
    }

    public void setUsername(String username) {
      this.username = username;
    }

    public BigDecimal getBaseBet() {
      return baseBet;
    }

    public void setBaseBet(BigDecimal baseBet) {
      this.baseBet = baseBet;
    }

    public Integer getSpinNumber() {
      return spinNumber;
    }

    public void setSpinNumber(Integer spinNumber) {
      this.spinNumber = spinNumber;
    }

    public Map<String, String> getParametersMap() {
      return parametersMap == null ? (parametersMap = new HashMap<>()) : parametersMap;
    }
  }

  public static class UserToken {

    public String getToken(String uri) {

      String[] response = new HttpClient(uri).get();

      String header = response[0];

      String token = Tools.extractString(header, "token=", "&");

      return token;

    }
  }
}
