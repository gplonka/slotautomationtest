package com.kalamba.logging.properties;

import com.kalamba.tools.Tools;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class LoadProperties {


  public static Properties loadProperties(String path) {
    return loadProperties(path, new Properties.UserToken());
  }

  public static Properties loadProperties(String path, Properties.UserToken userToken) {

    Properties.Parameters parameters = (Properties.Parameters) Tools
        .mapYamlToObject(path, new Properties.Parameters());

    parameters.getParametersMap().put("gameCode", parameters.getGameCode());
    parameters.getParametersMap().put("username", parameters.getUsername());
    parameters.getParametersMap().put("currency", parameters.getCurrency());

    Properties properties = (Properties) Tools
        .mapYamlToObject(LoadProperties.propertiesPath, new Properties());

    String httpUri = Tools.fillParameter(properties.getHttpUri(), parameters.getParametersMap());
    String token = userToken.getToken(httpUri);

    parameters.setToken(token);
    parameters.getParametersMap().put("token", parameters.getToken());

    String webSocketUri = Tools
        .fillParameter(properties.getWebSocketUri(), parameters.getParametersMap());
    String opeGameRequest = Tools
        .fillParameter(properties.getOpenGameRequest(), parameters.getParametersMap());
    String authenticateRequest = Tools
        .fillParameter(properties.getAuthenticateRequest(), parameters.getParametersMap());

    properties.setWebSocketUri(webSocketUri);
    properties.setOpenGameRequest(opeGameRequest);
    properties.setAuthenticateRequest(authenticateRequest);
    properties.setHttpUri(httpUri);

    properties.setParameters(parameters);

    return properties;
  }

  public static void loadGameRequestsParameters(String sourceJson, Properties properties) {

    String baseBet = Tools
        .getElement(sourceJson, LoadProperties.baseBetPath);
    String betMultiplier = Tools
        .getElement(sourceJson, LoadProperties.betMultiplierPath);

    properties.getParameters().getParametersMap().put("baseBet", baseBet);
    properties.getParameters().getParametersMap().put("betMultiplier", betMultiplier);

    properties.getParameters().setBaseBet(new BigDecimal(baseBet));
    properties.getParameters().setBetMultiplier(Integer.valueOf(betMultiplier));

    String baseSpinRequest = Tools.fillParameter(properties.getBaseSpinRequest(),
        properties.getParameters().getParametersMap());
    String freeSpinRequest = Tools.fillParameter(properties.getFreeSpiRequest(),
        properties.getParameters().getParametersMap());

    properties.setBaseSpinRequest(baseSpinRequest);
    properties.setFreeSpiRequest(freeSpinRequest);
  }

  private static Map<String, Integer> getGameCodeGameIdMap() {

    Map<String, Integer> gameCodeGameIdMap = new HashMap<>();

    gameCodeGameIdMap.put("wildmine", 26);

    return gameCodeGameIdMap;
  }

  public static String getForceOutcomePostBody(Properties p) {

    String user = "username=" + p.getParameters().getUsername() + "_kalamba&";
    String gameId = "gameId=" + getGameCodeGameIdMap().get(p.getParameters().getGameCode());

    return user + gameId + "&action=FreeSpins&acc=";
  }

  public static final String balancePath = "body.balance";
  public static final String winPath = "body.data.gameRoundResult.win";
  public static final String freeSpinPath = "body.data.gameState.freeSpinsState";
  public static final String freeSpinRemainingPath = "body.data.gameState.freeSpinsState.freeSpinsRemaining";
  public static final String eventPath = "body.event";
  public static final String SPIN_RESULT = "SPIN_RESULT";
  public static final String OPEN_GAME = "OPEN_GAME";
  public static final String baseBetPath = "body.data.additionalConfigData.defaultBet.baseBet";
  public static final String betMultiplierPath = "body.data.additionalConfigData.defaultBet.betMultiplier";
  public static final String propertiesPath = "/rgs.properties";
  public static final String forceOutcomeUri = "https://outcomes.dev.kalambagames.com/outcome";
}
