package com.kalamba.test;

import static org.junit.Assert.assertEquals;

import com.kalamba.tools.Tools;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.junit.BeforeClass;
import org.junit.Test;

public class ToolsTest {

  private static File temp;

  @BeforeClass
  public static void createTempFile() throws IOException {

    try (BufferedWriter writer = new BufferedWriter(
        new FileWriter(temp = File.createTempFile("tempFile", ".yml")))) {

      writer.write("name: \"xyz\"");
      writer.newLine();
      writer.write("age: 99");
    }
  }

  @Test
  public void fillParameterTest() {

    Map<String, String> map = new HashMap<>();

    map.put("par1", "A");
    map.put("par2", "B");
    map.put("par3", "C");

    String A = Tools.fillParameter("test[par1]test test[par2]", map);
    String B = Tools.fillParameter("test[par3]t", map);

    assertEquals("testAtest testB", A);
    assertEquals("testCt", B);
  }

  @Test
  public void mapYamlToObjectTest() throws IOException {

    Person p = (Person) Tools.mapYamlToObject(temp.getAbsolutePath(), new ToolsTest.Person());
    BufferedReader writer = new BufferedReader(
        new FileReader(temp) {
        });

    assertEquals("xyz", p.name);
    assertEquals(Integer.valueOf(99), p.age);
  }

  @Test(expected = RuntimeException.class)
  public void mapYamlToObjectExceptionTest() {
    Tools.mapYamlToObject("", new ToolsTest.Person());
  }

  @Test
  public void getJsonElementTest() {
    String element = Tools.getElement(jsonTest, "glossary.GlossDiv.GlossList.GlossEntry.ID");

    assertEquals("SGML", element);
  }

  @Test
  public void isPathExistTest() {
    Boolean exist = Tools.isPathExist(jsonTest, "glossary.GlossDiv.GlossList.GlossEntry.ID");
    Boolean notExist = Tools.isPathExist(jsonTest, "glossary.GlossDiv.GlossList.GlossEntry.I");

    assertEquals(true, exist);
    assertEquals(false, notExist);
  }

  @Test
  public void extractStringTest() {

    String s = Tools.extractString("asdfg4321", "s", "2");

    assertEquals("dfg43", s);
  }


  private static class Person {

    public String name;
    public Integer age;
  }


  private final String jsonTest = "{   \"glossary\":{      \"title\":\"example glossary\",\n" +
      "      \"GlossDiv\":{         \"title\":\"S\",\n" +
      "         \"GlossList\":{            \"GlossEntry\":{               \"ID\":\"SGML\",\n" +
      "               \"SortAs\":\"SGML\",\n" +
      "               \"GlossTerm\":\"Standard Generalized Markup Language\",\n" +
      "               \"Acronym\":\"SGML\",\n" +
      "               \"Abbrev\":\"ISO 8879:1986\",\n" +
      "               \"GlossDef\":{                  \"para\":\"com.kalamba.RunTest.A meta-markup language, used to create markup languages such as DocBook.\",\n"
      +
      "                  \"GlossSeeAlso\":[\n" +
      "                     \"GML\",\n" +
      "                     \"XML\"\n" +
      "                  \n" +
      "]\n" +
      "               \n" +
      "},\n" +
      "               \"GlossSee\":\"markup\"\n" +
      "            \n" +
      "}\n" +
      "         \n" +
      "}\n" +
      "      \n" +
      "}\n" +
      "   \n" +
      "}\n" +
      "}";
}


