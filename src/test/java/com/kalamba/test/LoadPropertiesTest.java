package com.kalamba.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kalamba.logging.properties.LoadProperties;
import com.kalamba.logging.properties.Properties;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.junit.BeforeClass;
import org.junit.Test;

public class LoadPropertiesTest {


  private static File temp;

  @BeforeClass
  public static void createTempFile() throws IOException {

    try (BufferedWriter writer = new BufferedWriter(
        new FileWriter(temp = File.createTempFile("tempFile", ".yml")))) {

      writer.write("gameCode: \"game\"");
      writer.newLine();
      writer.write("username: \"grzeg\"");
      writer.newLine();
      writer.write("spinNumber: 11");
      writer.newLine();
      writer.write("currency: \"USD\"");
    }
  }


  @Test
  public void pathsTest() {

    assertEquals("body.balance", LoadProperties.balancePath);
    assertEquals("body.data.gameRoundResult.win", LoadProperties.winPath);
    assertEquals("body.data.gameState.freeSpinsState", LoadProperties.freeSpinPath);
    assertEquals("body.data.gameState.freeSpinsState.freeSpinsRemaining",
        LoadProperties.freeSpinRemainingPath);
    assertEquals("body.event", LoadProperties.eventPath);
    assertEquals("SPIN_RESULT", LoadProperties.SPIN_RESULT);
    assertEquals("OPEN_GAME", LoadProperties.OPEN_GAME);
    assertEquals("body.data.additionalConfigData.defaultBet.baseBet", LoadProperties.baseBetPath);
    assertEquals("body.data.additionalConfigData.defaultBet.betMultiplier",
        LoadProperties.betMultiplierPath);
  }

  @Test
  public void propertiesTest() {

    Properties.UserToken userToken = mock(Properties.UserToken.class);
    when(userToken.getToken(anyString())).thenReturn("tokenTest");

    Properties p = LoadProperties.loadProperties(temp.getAbsolutePath(), userToken);
    LoadProperties.loadGameRequestsParameters(openGameResponse, p);

    assertEquals(baseSpinRequest, p.getBaseSpinRequest());
    assertEquals(wsUri, p.getWebSocketUri());
    assertEquals(openGameRequest, p.getOpenGameRequest());
    assertEquals(httpUri, p.getHttpUri());
  }


  private final String baseSpinRequest = "{ \"body\": { \"data\": { \"bet\": { \"baseBet\": 116, \"betMultiplier\": 1 } }, \"seqId\": 1, \"action\": \"SPIN\" }, \"header\": { \"cId\": 3, \"dType\": 2, \"mId\": 4, \"name\": \"GameAction\", \"dId\": 1 } }";
  private final String wsUri = "wss://gameserver-ci-api.kalambagames.com/oryxreplica/?cageCode=TestCage&gameCode=game&operatorCode=kalamba&token=tokenTest&username=grzeg";
  private final String openGameRequest = "{ \"body\": { \"gameCode\": game }, \"header\": { \"cId\": 2, \"dType\": 1, \"mId\": 2, \"name\": \"OpenGame\" } }";
  private final String openGameResponse = "{\"body\":{\"data\":{\"additionalConfigData\":{\"defaultBet\":{\"baseBet\":116,\"betMultiplier\":1}}},\"balance\":19950}}";
  private final String httpUri = "https://kalamba.integration.dev.kalambagames.com/games/url/?gameCode=game&currency=USD&cageCode=TestCage&username=grzeg";

}
