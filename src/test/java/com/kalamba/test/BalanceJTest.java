package com.kalamba.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.kalamba.logging.properties.LoadProperties;
import com.kalamba.logging.properties.Properties;
import com.kalamba.slot.service.Slot;
import com.kalamba.test.slot.game.BalanceTest;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

public class BalanceJTest {


  private static Properties properties;

  @BeforeClass
  public static void loadProperties() throws IOException {

    Properties.UserToken userToken = mock(Properties.UserToken.class);
    when(userToken.getToken(anyString())).thenReturn("");

    properties = LoadProperties.loadProperties(createTempFile().getAbsolutePath(), userToken);

  }

  @Test
  public void balanceTest() {

    Slot slot = mock(Slot.class);

    when(slot.baseSpin()).thenReturn(baseSpinResp);
    when(slot.openGame()).thenReturn(openGameResponse);
    Mockito.doNothing().when(slot).closeGame();

    BalanceTest balanceTest = new BalanceTest(slot, properties);
    callOpenGameMethod(balanceTest);
    balanceTest.completeBalanceTest();

    assertEquals(new BigDecimal(18960), balanceTest.getCountedBalanceAfterTest());
  }


  @Test
  public void balanceFreeSpinTest() {

    Slot slot = mock(Slot.class);

    when(slot.openGame()).thenReturn(openGameResponse);
    when(slot.baseSpin()).thenReturn(baseSpinRespWithFreeSpin);
    when(slot.freeSpin()).thenReturn(freeSpinResp);

    BalanceTest balanceTest = new BalanceTest(slot, properties);
    callOpenGameMethod(balanceTest);
    balanceTest.completeBalanceTest();

    assertEquals(new BigDecimal(18933), balanceTest.getCountedBalanceAfterTest());
  }

  private static File createTempFile() throws IOException {

    File temp;

    try (BufferedWriter writer = new BufferedWriter(
        new FileWriter(temp = File.createTempFile("tempFile", ".yml")))) {

      writer.write("gameCode: \"game\"");
      writer.newLine();
      writer.write("username: \"grzeg\"");
      writer.newLine();
      writer.write("spinNumber: 9");
      writer.newLine();
      writer.write("currency: \"USD\"");
    }

    return temp;

  }

  private void callOpenGameMethod(BalanceTest balanceTest) {

    try {
      Method method = balanceTest.getClass().getDeclaredMethod("openGame");
      method.setAccessible(true);
      method.invoke(balanceTest);
    } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
      e.printStackTrace();
    }
  }

  private final String baseSpinRespWithFreeSpin = "{\"body\":{\"data\":{\"gameRoundResult\":{\"win\":1},\"gameState\":{\"freeSpinsState\":{\"freeSpinsRemaining\":0,\"freeSpinsPlayed\":3,\"freeSpinsTotalWin\":492}}},\"balance\":4}}";
  private final String freeSpinResp = "{\"body\":{\"data\":{\"gameRoundResult\":{\"win\":2},\"gameState\":{\"freeSpinsState\":{\"freeSpinsRemaining\":0,\"freeSpinsPlayed\":3,\"freeSpinsTotalWin\":492}}},\"balance\":4}}";
  private final String baseSpinResp = "{\"body\":{\"event\":\"SPIN_RESULT\",\"data\":{\"gameRoundResult\":{\"win\":6}},\"balance\":4}}";
  private final String openGameResponse = "{\"body\":{\"data\":{\"additionalConfigData\":{\"defaultBet\":{\"baseBet\":116,\"betMultiplier\":1}}},\"balance\":19950}}";

}

